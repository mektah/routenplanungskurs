#

import sys

COORDINATES_FILE="windischleuba_coordinates"
INPUT_FILE="plan_Windischleuba.svg"

def readCoordinates(path=COORDINATES_FILE, node_range=300):
  """Read coordinates file

  The format is that each line contains three integers: the
  node number, the x-coordinates, and the y-coordinate. Lines
  not complying with format are ignored (they are considered
  comments).

  """
  coordinates = [ (None, None) for _ in range(node_range+1) ]
  with open(path, 'r') as f:
    for line in f:
      tokens = line.strip().split()
      try:
        k = int(tokens[0])
        x = int(tokens[1])
        y = int(tokens[2])
        coordinates[k] = (x, y)
      except:
        pass

  return coordinates

def printCoordinates(coordinates):
  """Debug-print a list of coordinates"""
  for k in range(len(coordinates)):
    (x, y) = coordinates[k]
    if x is not None:
      print("%2d (%4d, %4d)" % (k, x, y))

def getPath(coordinates, nodeidxs):
  """Get the svg representation of a path

  Given the node coordinates and a list of node
  indices, return a line in svg format showing this path.

  """
  d =""
  fmt = "M %d %d"
  for idx in nodeidxs:
    d += fmt % coordinates[idx]
    fmt = " L %d %d"

  return ('<path id="theRoute" d="%s" stroke="red"'
          ' fill="none" stroke-width="2" />' % (d,))

def getVisual(coordinates, nodeidxs, inputfile=INPUT_FILE):
  """Get a plan with the path drawn into it"""
  result = ""
  with open(inputfile, 'r') as f:
    for line in f:
      endpos = line.find("</svg>")
      if endpos >= 0:
        result += line[:endpos]
        result += getPath(coordinates, nodeidxs)
        result += line[endpos:]
      else:
        result += line

  return result


if __name__ == "__main__":
  coordinates = readCoordinates()
  nodeidxs = [int(x) for x in sys.argv[1:]]
  print(getVisual(coordinates, nodeidxs))
